from django.shortcuts import render, get_object_or_404
from Orion_review.models import Article, Commentaire
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .filters import ArticleFilter
from Orion_review.form import CommentaireForm

def index(request):

    articles = Article.objects.order_by('-id').all()

    paginator = Paginator(articles, 2) # Show 2 contacts per page

    page = request.GET.get('page')
    try:
        articlesP = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        articlesP = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        articlesP = paginator.page(paginator.num_pages)
    return render(request, 'index.html', {'latests_articles': articlesP})

def article(request, slug):

    article = get_object_or_404(Article, slug=slug)

    commentaires = Commentaire.objects.filter(article_id=article.id)

    commentaire = Commentaire(article_id=article.id)

    form = CommentaireForm(request.POST , instance=commentaire)
    if form.is_valid():
        form.save()
    form = CommentaireForm()
    return render(request, 'article.html', {'article': article, 'form':form, 'commentaires': commentaires})

def search(request):
    article_list = Article.objects.all()
    article_filter = ArticleFilter(request.GET, queryset=article_list)

    return render(request, 'search.html', {'filter': article_filter})
