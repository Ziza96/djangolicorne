from django.urls import path
from Orion_review import views
app_name = 'Orion_review'

urlpatterns = [
    path('', views.index, name='index'),
    path('article/<slug:slug>', views.article, name='article'),
    path('search', views.search, name='search'),
]
