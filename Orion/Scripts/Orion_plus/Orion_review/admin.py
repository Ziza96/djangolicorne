from django.contrib import admin
from Orion_review.models import *

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('name', 'release_date', )
    search_fields = ('name', )

class CommentaireAdmin(admin.ModelAdmin):
    list_display = ('content', 'release_date', )
    search_fields = ('content', )

admin.site.register(Article, ArticleAdmin)
admin.site.register(Commentaire, CommentaireAdmin)
