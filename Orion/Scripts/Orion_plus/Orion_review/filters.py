from Orion_review.models import Article
import django_filters

class ArticleFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains', label='Titre')
    class Meta:
        model = Article
        fields = ['name', ]
