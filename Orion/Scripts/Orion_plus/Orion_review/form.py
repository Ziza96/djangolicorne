from django import forms
from .models import Commentaire

class CommentaireForm(forms.ModelForm):
    class Meta:
        model = Commentaire
        labels = {
            'content': ('Commentaire'),
            'author': ('Pseudo'),
        }
        exclude  = ('article',)
