from django.apps import AppConfig


class OrionReviewConfig(AppConfig):
    name = 'Orion_review'
