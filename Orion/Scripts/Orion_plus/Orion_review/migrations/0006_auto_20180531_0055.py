# Generated by Django 2.0.2 on 2018-05-30 22:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Orion_review', '0005_auto_20180529_2242'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commentaire',
            name='author',
            field=models.CharField(max_length=50),
        ),
    ]
