# Generated by Django 2.0.2 on 2018-05-31 07:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Orion_review', '0006_auto_20180531_0055'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commentaire',
            name='article',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Orion_review.Article'),
        ),
    ]
