from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User

class Article(models.Model):
    name = models.CharField(max_length=150)
    content = models.TextField()
    slug = models.SlugField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    release_date = models.DateTimeField(auto_now_add=True)
    picture = models.ImageField()
    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'
    def  __str__(self):
        return self.name


class Commentaire(models.Model):
    content = models.TextField()
    author = models.CharField(max_length=50)
    release_date = models.DateTimeField(auto_now_add=True)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
