from django.test import TestCase
from django.urls import reverse
from django.db.models.query import QuerySet
from Orion_review.models import Article

class WebsiteTestCase(TestCase):
    # fixtures = ['initial.json', ]
    def test_index_page(self):
        response = self.client.get(reverse('Orion_review:index'))
        self.assertContains(response, "Les dernières actualités")
        self.assertEqual(type(response.context['latest_arcticle']), QuerySet)
        self.assertEqual(len(response.context['latest_arcticle']), 3)
        self.failUnlessEqual(response.status_code, 200)
        self.assertTemplateUsed('index.html')

    def test_post_page(self):
        # Published news
        article = Article.objects.first()
        response = self.client.get(reverse('Orion_review:post', kwargs={'slug': article.slug}))
        self.assertContains(response, article.name)
        self.assertEqual(type(response.context['article']), Article)
        self.failUnlessEqual(response.status_code, 200)
        self.assertTemplateUsed('news/index.html')
